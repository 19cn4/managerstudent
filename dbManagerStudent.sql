drop database if exists manager_student;
create database manager_student;


use manager_student;

create table user(
id int not null primary key,
username varchar(6) not null,
pass varchar(6) not null
);


insert into user (id,username,pass) 
values (
1, 
'admin',
'admin'
);


select * from user where username = 'admin' and pass = 'admin';


use manager_student;
create table sinhVien (
msv int primary key,
gioiTinh char(3) not null,
hoTen nvarchar(50) not null,
birthday Date not null,
address nvarchar(255) not null,
phone_number varchar(255) null,
diemTK double 
);


create table monHoc (
id_mh int primary key,
id_cs int not null,
tenMh nvarchar(50) not null,
diemTB double
);

create table coSo (
id_cs int primary key auto_increment,
tenCS nvarchar(50) not null
);


insert into coSo(tenCS) values ('Tất cả');
insert into coSo(tenCS) values ('Cơ sở');
insert into coSo(tenCS) values ('Chuyên ngành');

create table diem(
msv int not null,
id_mh int not null,
diem double null,
foreign key (msv) references sinhVien(msv),
foreign key (id_mh) references  monHoc(id_mh)
);



-- use manager_student;
-- insert into sinhVien (msv, hoTen, gioiTinh, birthday, address,phone_number)
--  values 
--  (
--  101,
--  'Nguyen Van B',
--  'Nam',
--  '2001-10-20',
--  n'Nghệ An',
--  '0123456789'
--  );
--  insert into sinhVien (msv, hoTen, gioiTinh, birthday, address,phone_number)
--  values 
--  (
--  102,
--  'Nguyen Van C',
--  'Nam',
--  '2001-10-25',
--  n'Hà Nam','0987654321'
--  );

--  insert into sinhVien (msv, hoTen, gioiTinh, birthday, address,phone_number)
--  values 
--  (
--  103,
--  'Nguyen Van D',
--  'Nam',
--  '2001-10-30',
--  n'Hà Nội','1234567890'
--  );
 
 
--  insert into monHoc (id_mh, id_cs, tenMh, diemTB) value (
--  200, 
--  1,
--  n'java', 
--  9.6
--  );
--  
--   insert into monHoc (id_mh, id_cs, tenMh, diemTB) value (
--  201, 
-- 2,
--  n'C++', 
--  4.6
--  );
--  
--  
--  
--   insert into monHoc (id_mh, id_cs, tenMh, diemTB) value (
--  202, 
--  3,
--  n'php', 
--  7.6
--  );
 
 
--  select max(id_mh) from monHoc;
--  
-- select * from diem;

-- insert into diem(msv, id_mh, diem) values (
-- 101, 
-- 200, 
-- 8.5

-- );

-- insert into diem(msv, id_mh, diem) values (
-- 101, 
-- 201, 
-- 3.5

-- );

-- insert into diem(msv, id_mh, diem) values (
-- 102, 
-- 200, 
-- 9.5

-- );



-- select * from diem;


-- select diem from diem where msv = 101;

-- select * from monHoc;
-- select * from diem where msv = 101 and id_mh  = 200;
-- select * from monHoc;




